<?php


namespace App\Http\Controllers\View;


use App\Http\Controllers\Controller;
use App\Models\covid;
use Illuminate\Http\Request;

class CaseController extends Controller
{
    public function index( Request $request )
    {
        $covid = covid::all();

        $data = [];
        $data["case"] = $covid;

        return view( "frontend.case", $data);
    }

}

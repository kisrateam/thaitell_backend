<?php


namespace App\Http\Controllers\View;


use App\Http\Controllers\Controller;
use App\Models\Cases;
use App\Models\covid;
use Illuminate\Http\Request;

class MapController extends Controller
{
    public function index( Request $request )
    {
        $cases = Cases::all();

        $location=[];
        foreach ($cases as $case) {

            if ($case->source_lat != null) {
                $lo = [
                    "lat" => floatval( $case->source_lat ),
                    "lng" => floatval(  $case->source_long )
                ];
                array_push($location, $lo);
            }
        }

//        dd($location);

        $cnt = $cases->count();

        return view( "frontend.map", compact('location'))->with('cnt',$cnt);
    }

}

<?php


namespace App\Http\Controllers\View;


use App\Http\Controllers\Controller;
use App\Models\covid;
use App\Models\News;
use Carbon\Carbon;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function index( Request $request )
    {
        $latest = News::latest('update_date')->first();
        $latest->date = Carbon::parse($latest->update_date)->format('d F Y');

        $data = News::orderBy('update_date','desc')->where('id','!=', $latest->id)->get();
        $data->map(function ($q){
            $q->date = Carbon::parse($q->update_date)->format('d F Y');
        });


        return view( "frontend.news")->with("data", $data)->with("latest",$latest);
    }
    public function readmore($id)
    {
        $data = News::orderBy('update_date','desc')->where('id','!=', $id)->get();
        $data->map(function ($q){
            $q->date = Carbon::parse($q->update_date)->format('d F Y');
        });

        $latest = News::find($id);
        $latest->date = Carbon::parse($latest->update_date)->format('d F Y');

        return view( "frontend.readmore")->with("data", $data)->with("news", $latest);
    }

}

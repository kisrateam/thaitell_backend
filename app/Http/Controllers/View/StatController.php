<?php


namespace App\Http\Controllers\View;


use App\Http\Controllers\Controller;
use App\Models\Cases;
use App\Models\covid;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StatController extends Controller
{
    public function index( Request $request )
    {
        $cases = DB::table('cases')
            ->select('report_date', DB::raw('count(*) as total'))
            ->groupBy('report_date')
            ->get();

        $data = [];
        $total = [];
        $categories = [];
        foreach ($cases->all() as $case){
            array_push($categories, Carbon::parse($case->report_date)->format('d/m/yy'));
            array_push($total, $case->total);
        }


        $yourFirstChart["chart"] = array("type" => "line");
        $yourFirstChart["title"] = array("text" => "Total Coronavirus in Thailand");
        $yourFirstChart["xAxis"] = array("categories" => $categories);
        $yourFirstChart["yAxis"] = array("title" => array("text" => "Total Coronavirus in Thailand"));

        $yourFirstChart["series"] = [
            array("name" => "ผู้ติดเชื้อ", "data" => $total),
        ];

        $data["cases"] = $yourFirstChart;


        $cases = Cases::select('gender', DB::raw('count(*) as total'))
            ->groupBy('gender')
            ->get();

        $categories = [];
        foreach ($cases->all() as $case){
            array_push($categories,[
                "name" => $case->gender ?  $case->gender : "ไม่ระบุ",
                "y" => $case->total
            ]);
        }
        $data["casesByGender"] = $categories;


        $cases = Cases::select('age', DB::raw('count(*) as total'))
            ->groupBy('age')
            ->get();

        $i0 = 0;
        $i1 = 0;
        $i2 = 0;
        $i3 = 0;
        $i4 = 0;
        $i5 = 0;
        $i6 = 0;
        foreach ($cases->all() as $case){
            if($case->age > 0 && $case->age <= 10){
                $i0 += $case->total;
            } elseif ($case->age > 11 && $case->age <= 20){
                $i1 += $case->total;
            } elseif ($case->age > 21 && $case->age <= 30){
                $i2 += $case->total;
            } elseif ($case->age > 31 && $case->age <= 40){
                $i3 += $case->total;
            } elseif ($case->age > 41 && $case->age <= 50){
                $i4 += $case->total;
            } elseif ($case->age > 51 && $case->age <= 60){
                $i5 += $case->total;
            } elseif ($case->age > 60){
                $i6 += $case->total;
            }
        }

        $age = [
            ["0-10",$i0],
            ["11-20",$i1],
            ["21-30",$i2],
            ["31-40",$i3],
            ["41-50",$i4],
            ["51-60",$i5],
            ["60+",$i6],
        ];
        $data["casesByAge"] = $age;

        $cases = Cases::select('status', DB::raw('count(*) as total'))
            ->groupBy('status')
            ->get();

        $categories = [];
        foreach ($cases->all() as $case){
            array_push($categories,[
                "name" => $case->status ?  $case->status : "ไม่ระบุ",
                "y" => $case->total
            ]);
        }
        $data["casesByStatus"] = $categories;

        $cases = Cases::select('nation', DB::raw('count(*) as total'))
            ->groupBy('nation')
            ->get();

        $categories = [];
        $sum = [];
        $thai = 0;
        $foreigner = 0;
        foreach ($cases->all() as $case){
            if($case->nation == 'thai' ){
                $thai+= $case->total;
            } else {
                $foreigner+= $case->total;
            }
            $key = "อื่นๆ";

            if($case->nation == 'china' ){
                $key = "จีน";
            }else if($case->nation == 'thai' ){
                $key = "ไทย";
            }else if($case->nation == null){
                $key = "ไม่ระบุ";
            }


            if(!array_key_exists($key,$sum)){
                $sum[$key] = 0;
            }
            $sum[$key] += $case->total;
        }


        $categories = [
            [
                "name" => "ไทย",
                "y" =>  $sum["ไทย"]
            ],
            [
                "name" => "จีน",
                "y" =>  $sum["จีน"]
            ],
            [
                "name" => "อื่นๆ",
                "y" =>  $sum["อื่นๆ"]
            ],
            [
                "name" => "ไม่ระบุ",
                "y" =>  $sum["ไม่ระบุ"]
            ],
        ];



        $data["casesByNation"] = $categories;
        $data["thai"] = $thai;
        $data["foreigner"] = $foreigner;

        $cases = Cases::all();
        $data["sum"] = $cases->count();
        $update_date = Cases::latest('created_at')->first()->created_at;
        $data["datetime"] = Carbon::parse($update_date)->format('d F Y h:m A');

        $urls = [];
        $cases->map(function ($q) use (&$urls){
            if ($q->source_url && !in_array($q->source_url,$urls)) {
                array_push($urls, $q->source_url);
            }
            if ($q->source_url_2 && !in_array($q->source_url_2,$urls)) {
                array_push($urls, $q->source_url_2);
            }
            return $q;
        });

        return view( "frontend.stat", compact('data'))->with('casesByStatus', $data['casesByStatus'])->with('data', $data)->with('urls', $urls);
    }
}

<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class covid extends Sximo  {
	
	protected $table = 'cases';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT cases.* FROM cases  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE cases.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cases', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('age')->nullable();
            $table->string('gender');
            $table->string('nation');
            $table->string('hospital')->nullable();
            $table->string('thai_source_location')->nullable();
            $table->float('source_lat')->nullable();
            $table->float('source_long')->nullable();
            $table->string('status')->nullable();
            $table->integer('related_patient_id')->nullable();
            $table->tinyInteger('from_china')->nullable();
            $table->string('source')->nullable();
            $table->longText('detect_process')->nullable();
            $table->string('source_url')->nullable();
            $table->string('source_url_2')->nullable();
            $table->dateTime('found_date')->nullable();
            $table->dateTime('report_date')->nullable();
            $table->dateTime('update_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cases');
    }
}

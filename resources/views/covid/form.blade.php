<div class="form-ajax-box">
{!! Form::open(array('url'=>'covid?return='.$return, 'class'=>'form-horizontal form-material validated','files' => true , 'parsley-validate'=>'','novalidate'=>' ','id'=> 'covidFormAjax')) !!}

	<div class="toolbar-nav">	
		<div class="row">	
			<div class="col-sm-6 ">	
				<button type="submit" class="btn btn-sm  " name="apply">{{ Lang::get('core.sb_apply') }} </button>
				<button type="submit" class="btn btn-sm  " name="save">  {{ Lang::get('core.sb_save') }} </button>
			</div>	
			<div class="col-md-6 text-right">
				<a href="javascript://ajax" onclick="ajaxViewClose('#{{ $pageModule }}')" class="tips btn btn-sm  " title="{{ __('core.btn_back') }}" ><i class="fa  fa-times"></i></a>				
			</div>
					
		</div>
	</div>	
		<div class="card">
			<div class="card-body">

	
	<div class="row"><div class="col-md-12">
						<fieldset><legend> Covid</legend>
				{!! Form::hidden('id', $row['id']) !!}					
									  <div class="form-group row  " >
										<label for="Age" class=" control-label col-md-4 text-left"> Age </label>
										<div class="col-md-6">
										  <input  type='text' name='age' id='age' value='{{ $row['age'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group row  " >
										<label for="Gender" class=" control-label col-md-4 text-left"> Gender </label>
										<div class="col-md-6">
										  <input  type='text' name='gender' id='gender' value='{{ $row['gender'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group row  " >
										<label for="Nation" class=" control-label col-md-4 text-left"> Nation </label>
										<div class="col-md-6">
										  <input  type='text' name='nation' id='nation' value='{{ $row['nation'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group row  " >
										<label for="Hospital" class=" control-label col-md-4 text-left"> Hospital </label>
										<div class="col-md-6">
										  <input  type='text' name='hospital' id='hospital' value='{{ $row['hospital'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group row  " >
										<label for="Thai Source Location" class=" control-label col-md-4 text-left"> Thai Source Location </label>
										<div class="col-md-6">
										  <input  type='text' name='thai_source_location' id='thai_source_location' value='{{ $row['thai_source_location'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group row  " >
										<label for="Source Lat" class=" control-label col-md-4 text-left"> Source Lat </label>
										<div class="col-md-6">
										  <input  type='text' name='source_lat' id='source_lat' value='{{ $row['source_lat'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group row  " >
										<label for="Source Long" class=" control-label col-md-4 text-left"> Source Long </label>
										<div class="col-md-6">
										  <input  type='text' name='source_long' id='source_long' value='{{ $row['source_long'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group row  " >
										<label for="Status" class=" control-label col-md-4 text-left"> Status </label>
										<div class="col-md-6">
										  <input  type='text' name='status' id='status' value='{{ $row['status'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group row  " >
										<label for="Related Patient Id" class=" control-label col-md-4 text-left"> Related Patient Id </label>
										<div class="col-md-6">
										  <input  type='text' name='related_patient_id' id='related_patient_id' value='{{ $row['related_patient_id'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group row  " >
										<label for="From China" class=" control-label col-md-4 text-left"> From China </label>
										<div class="col-md-6">
										  <input  type='text' name='from_china' id='from_china' value='{{ $row['from_china'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group row  " >
										<label for="Source" class=" control-label col-md-4 text-left"> Source </label>
										<div class="col-md-6">
										  <input  type='text' name='source' id='source' value='{{ $row['source'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group row  " >
										<label for="Detect Process" class=" control-label col-md-4 text-left"> Detect Process </label>
										<div class="col-md-6">
										  <input  type='text' name='detect_process' id='detect_process' value='{{ $row['detect_process'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group row  " >
										<label for="Source Url" class=" control-label col-md-4 text-left"> Source Url </label>
										<div class="col-md-6">
										  <input  type='text' name='source_url' id='source_url' value='{{ $row['source_url'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group row  " >
										<label for="Source Url 2" class=" control-label col-md-4 text-left"> Source Url 2 </label>
										<div class="col-md-6">
										  <input  type='text' name='source_url_2' id='source_url_2' value='{{ $row['source_url_2'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group row  " >
										<label for="Found Date" class=" control-label col-md-4 text-left"> Found Date </label>
										<div class="col-md-6">
										  
					{!! Form::text('found_date', $row['found_date'],array('class'=>'form-control form-control-sm datetime')) !!}
				 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group row  " >
										<label for="Report Date" class=" control-label col-md-4 text-left"> Report Date </label>
										<div class="col-md-6">
										  
					{!! Form::text('report_date', $row['report_date'],array('class'=>'form-control form-control-sm datetime')) !!}
				 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group row  " >
										<label for="Update Date" class=" control-label col-md-4 text-left"> Update Date </label>
										<div class="col-md-6">
										  
					{!! Form::text('update_date', $row['update_date'],array('class'=>'form-control form-control-sm datetime')) !!}
				 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset></div></div>									
						
	<input type="hidden" name="action_task" value="save" />

	</div>
	

</div>		
{!! Form::close() !!}
</div>

<style type="text/css">
	.modal-body .form-ajax-box {
		margin: -15px;
	}
</style>
@include('sximo.module.template.ajax.formjavascript')
<script type="text/javascript">
$(document).ready(function() { 
	 
	
	 	
	 
	
	var form = $('#covidFormAjax'); 
	form.parsley();
	form.submit(function(){
		
		if(form.parsley().isValid()){			
			var options = { 
				dataType:      'json', 
				beforeSubmit :  function() {
				},
				success		:   function(data) {

					if(data.status == 'success')
					{
						ajaxViewClose('#{{ $pageModule }}');
						ajaxFilter('#{{ $pageModule }}','{{ $pageUrl }}/data');
						notyMessage(data.message);	
						$('#sximo-modal').modal('hide');	
					} else {
						notyMessageError(data.message);	
						return false;
					}	

				}  
			}  
			$(this).ajaxSubmit(options); 
			return false;
						
		} else {
			return false;
		}		
	
	});

});

</script>		 


		 {!! Form::open(array('url'=>'covid/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="row"><div class="col-md-12">
						<fieldset><legend> Covid</legend>
				{!! Form::hidden('id', $row['id']) !!}					
									  <div class="form-group row  " >
										<label for="Age" class=" control-label col-md-4 text-left"> Age </label>
										<div class="col-md-6">
										  <input  type='text' name='age' id='age' value='{{ $row['age'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group row  " >
										<label for="Gender" class=" control-label col-md-4 text-left"> Gender </label>
										<div class="col-md-6">
										  <input  type='text' name='gender' id='gender' value='{{ $row['gender'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group row  " >
										<label for="Nation" class=" control-label col-md-4 text-left"> Nation </label>
										<div class="col-md-6">
										  <input  type='text' name='nation' id='nation' value='{{ $row['nation'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group row  " >
										<label for="Hospital" class=" control-label col-md-4 text-left"> Hospital </label>
										<div class="col-md-6">
										  <input  type='text' name='hospital' id='hospital' value='{{ $row['hospital'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group row  " >
										<label for="Thai Source Location" class=" control-label col-md-4 text-left"> Thai Source Location </label>
										<div class="col-md-6">
										  <input  type='text' name='thai_source_location' id='thai_source_location' value='{{ $row['thai_source_location'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group row  " >
										<label for="Source Lat" class=" control-label col-md-4 text-left"> Source Lat </label>
										<div class="col-md-6">
										  <input  type='text' name='source_lat' id='source_lat' value='{{ $row['source_lat'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group row  " >
										<label for="Source Long" class=" control-label col-md-4 text-left"> Source Long </label>
										<div class="col-md-6">
										  <input  type='text' name='source_long' id='source_long' value='{{ $row['source_long'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group row  " >
										<label for="Status" class=" control-label col-md-4 text-left"> Status </label>
										<div class="col-md-6">
										  <input  type='text' name='status' id='status' value='{{ $row['status'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group row  " >
										<label for="Related Patient Id" class=" control-label col-md-4 text-left"> Related Patient Id </label>
										<div class="col-md-6">
										  <input  type='text' name='related_patient_id' id='related_patient_id' value='{{ $row['related_patient_id'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group row  " >
										<label for="From China" class=" control-label col-md-4 text-left"> From China </label>
										<div class="col-md-6">
										  <input  type='text' name='from_china' id='from_china' value='{{ $row['from_china'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group row  " >
										<label for="Source" class=" control-label col-md-4 text-left"> Source </label>
										<div class="col-md-6">
										  <input  type='text' name='source' id='source' value='{{ $row['source'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group row  " >
										<label for="Detect Process" class=" control-label col-md-4 text-left"> Detect Process </label>
										<div class="col-md-6">
										  <input  type='text' name='detect_process' id='detect_process' value='{{ $row['detect_process'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group row  " >
										<label for="Source Url" class=" control-label col-md-4 text-left"> Source Url </label>
										<div class="col-md-6">
										  <input  type='text' name='source_url' id='source_url' value='{{ $row['source_url'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group row  " >
										<label for="Source Url 2" class=" control-label col-md-4 text-left"> Source Url 2 </label>
										<div class="col-md-6">
										  <input  type='text' name='source_url_2' id='source_url_2' value='{{ $row['source_url_2'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group row  " >
										<label for="Found Date" class=" control-label col-md-4 text-left"> Found Date </label>
										<div class="col-md-6">
										  
					{!! Form::text('found_date', $row['found_date'],array('class'=>'form-control form-control-sm datetime')) !!}
				 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group row  " >
										<label for="Report Date" class=" control-label col-md-4 text-left"> Report Date </label>
										<div class="col-md-6">
										  
					{!! Form::text('report_date', $row['report_date'],array('class'=>'form-control form-control-sm datetime')) !!}
				 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group row  " >
										<label for="Update Date" class=" control-label col-md-4 text-left"> Update Date </label>
										<div class="col-md-6">
										  
					{!! Form::text('update_date', $row['update_date'],array('class'=>'form-control form-control-sm datetime')) !!}
				 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset></div></div>

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 

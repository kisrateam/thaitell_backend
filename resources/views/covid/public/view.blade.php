<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Id', (isset($fields['id']['language'])? $fields['id']['language'] : array())) }}</td>
						<td>{{ $row->id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Age', (isset($fields['age']['language'])? $fields['age']['language'] : array())) }}</td>
						<td>{{ $row->age}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Gender', (isset($fields['gender']['language'])? $fields['gender']['language'] : array())) }}</td>
						<td>{{ $row->gender}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nation', (isset($fields['nation']['language'])? $fields['nation']['language'] : array())) }}</td>
						<td>{{ $row->nation}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Hospital', (isset($fields['hospital']['language'])? $fields['hospital']['language'] : array())) }}</td>
						<td>{{ $row->hospital}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Thai Source Location', (isset($fields['thai_source_location']['language'])? $fields['thai_source_location']['language'] : array())) }}</td>
						<td>{{ $row->thai_source_location}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Source Lat', (isset($fields['source_lat']['language'])? $fields['source_lat']['language'] : array())) }}</td>
						<td>{{ $row->source_lat}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Source Long', (isset($fields['source_long']['language'])? $fields['source_long']['language'] : array())) }}</td>
						<td>{{ $row->source_long}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Status', (isset($fields['status']['language'])? $fields['status']['language'] : array())) }}</td>
						<td>{{ $row->status}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Related Patient Id', (isset($fields['related_patient_id']['language'])? $fields['related_patient_id']['language'] : array())) }}</td>
						<td>{{ $row->related_patient_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('From China', (isset($fields['from_china']['language'])? $fields['from_china']['language'] : array())) }}</td>
						<td>{{ $row->from_china}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Source', (isset($fields['source']['language'])? $fields['source']['language'] : array())) }}</td>
						<td>{{ $row->source}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Detect Process', (isset($fields['detect_process']['language'])? $fields['detect_process']['language'] : array())) }}</td>
						<td>{{ $row->detect_process}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Source Url', (isset($fields['source_url']['language'])? $fields['source_url']['language'] : array())) }}</td>
						<td>{{ $row->source_url}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Source Url 2', (isset($fields['source_url_2']['language'])? $fields['source_url_2']['language'] : array())) }}</td>
						<td>{{ $row->source_url_2}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Found Date', (isset($fields['found_date']['language'])? $fields['found_date']['language'] : array())) }}</td>
						<td>{{ $row->found_date}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Report Date', (isset($fields['report_date']['language'])? $fields['report_date']['language'] : array())) }}</td>
						<td>{{ $row->report_date}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Update Date', (isset($fields['update_date']['language'])? $fields['update_date']['language'] : array())) }}</td>
						<td>{{ $row->update_date}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Created At', (isset($fields['created_at']['language'])? $fields['created_at']['language'] : array())) }}</td>
						<td>{{ $row->created_at}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Updated At', (isset($fields['updated_at']['language'])? $fields['updated_at']['language'] : array())) }}</td>
						<td>{{ $row->updated_at}} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	
<style>
    .head-bar {
        padding: 0.5rem;
        color: white;
        margin-top: -2px;
        background: linear-gradient(159.29deg, #D43731 43.73%, #D95A69 91.36%);
    }

    .menu-wrapper {
        position:fixed !important;
        z-index: 1;
        width: 100%;
        margin-bottom: 2rem;
        top: 3.6rem;
        right: 0;
        left: 0;
    }
</style>

<div class="menu-wrapper">
    <div align="center" class="head-bar">
        <div class="row">
            <div class="col-2 m-auto">
                @if (strpos($_SERVER['REQUEST_URI'] , 'read') !== false)
                    <a href="/news" style="color: white;">
                        <i class="fas fa-chevron-left"></i>
                    </a>
                    @elseif ($_SERVER['REQUEST_URI'] !== '/map'&& $_SERVER['REQUEST_URI'] !== '/')
                    <a href="/" style="color: white;">
                        <i class="fas fa-chevron-left"></i>
                    </a>
                @endif
            </div>
            <div class="col-8 text-center my-auto" style="font-size: 1rem;">
                @if ( $_SERVER['REQUEST_URI'] === '/map' || $_SERVER['REQUEST_URI'] === '/')
                    Covid-19 in Thailand
                @elseif ($_SERVER['REQUEST_URI'] === '/news' || strpos($_SERVER['REQUEST_URI'] , 'news') )
                    News / ข่าว
                @elseif ($_SERVER['REQUEST_URI'] === '/stat')
                    Statistic / สถิติ
                @endif
            </div>
            <div class="col-2">

                <a class="navbar-toggler menu-head m-auto" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                    {{--                    <i class="fas fa-times close m-auto"></i>--}}
                    <i class="fas fa-ellipsis-v m-auto pt-1"></i>
                </a>
            </div>
        </div>
    </div>
    <div class="collapse navbar-collapse nav-menu" id="navbarTogglerDemo01">
        <ul class="navbar-nav mr-auto mt-lg-0 text-center"
             @if(strpos($_SERVER['REQUEST_URI'] , 'stat') !== false  || strpos($_SERVER['REQUEST_URI'] , 'news') !== false )
            style="margin-top: 40px;"
            @endif
        >
            <li class="nav-item p-1">
                <a class="nav-link @if($_SERVER['REQUEST_URI'] === 'map'|| $_SERVER['REQUEST_URI'] === '/') active @endif" style="color: black;" href="{{url('map')}}">Map / แผนที่</a>
            </li>
            <li class="nav-item p-1">
                <a class="nav-link @if($_SERVER['REQUEST_URI'] === 'stat') active @endif" style="color: black;" href="{{url('stat')}}">Statistic / สถิติ</a>
            </li>
            <li class="nav-item p-1">
                <a class="nav-link @if($_SERVER['REQUEST_URI'] === 'news') active @endif" style="color: black;" href="{{url('news')}}">News / ข่าว</a>
            </li>
        </ul>
    </div>
</div>


<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Prompt&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <title>THAI TELL</title>
</head>
<body >
<style type="text/css">
    body {
        font-family: 'Prompt', sans-serif;
    }

    .title-bar {
        padding: 2%;
        color: white;
        background: linear-gradient(163.69deg, #002952 48.26%, #0091B9 114.15%);
    }

    .main-content {
        /*margin-top: 4rem;*/
    }

    .page {
        margin-top: 6.2rem;
    }

    .navbar-brand {
        text-align: center;
    }
    .menu-head {
        background: transparent;
    }

    .nav-menu {
        background-image: linear-gradient(white, rgb(212,213,214));
        font-size: 15px;
        color: black !important;
    }

    /*.navbar-toggler>.close {*/
    /*    display:inline;*/
    /*}*/
    /*.navbar-toggler.collapsed>.close, .navbar-toggler:not(.collapsed)>.navbar-toggler-icon {*/
    /*    display:none;*/
    /*}*/
</style>
<nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark mx-auto flex-column">
    <div class="row">
        <div class="col">
            <a class="navbar-brand mx-auto" href="#">
                <img src="{{ asset('assets/images/thaitell_logo.png') }}" width="60" alt="">
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col">
            @include('frontend.layouts.menu-bar')
        </div>
    </div>
</nav>




<div class="main-content" id="main-content">
    <div class="page">
        @yield('content')
    </div>
</div>
</body>
</html>


@extends('frontend.layouts.none_main')
@section('content')
    <style>
        .stat {
            display: inline-block;
            position: absolute;
            bottom: 2vh;left:2vh;
            background-color: crimson;
            color: #fff;padding: 15px 30px;
            border-radius: 5px;opacity: 0.7;
            line-height: 10px;
        }
    </style>
    <div id="map" style="
    height: calc(100vh - 100px);display: block;
    position:relative;
    overflow: hidden;">

    </div>

    <div class="stat">
        <h6>{{$cnt}} ราย</h6>
    </div>
    <script>
        var map, infoWindow;
        var locations = {!! json_encode($location) !!}


        function initMap() {

            var styledMapType = new google.maps.StyledMapType(
            [
              {elementType: 'geometry', stylers: [{color: '#f2f3ea'}]},
              {elementType: 'labels.text.fill', stylers: [{color: '#343A40'}]},
              {
                featureType: 'administrative',
                elementType: 'geometry.stroke',
                stylers: [{color: '#f2f3ea'}]
              },
              {
                featureType: 'administrative.land_parcel',
                elementType: 'geometry.stroke',
                stylers: [{color: '#f2f3ea'}]
              },
              {
                featureType: 'administrative.land_parcel',
                elementType: 'labels.text.fill',
                stylers: [{color: '#f2f3ea'}]
              },
              {
                featureType: 'landscape.natural',
                elementType: 'geometry',
                stylers: [{color: '#f2f3ea'}]
              },
              {
                featureType: 'poi',
                elementType: 'geometry',
                stylers: [{color: '#f2f3ea'}]
              },
              {
                featureType: 'poi',
                elementType: 'labels.text.fill',
                stylers: [{color: '#343A40'}]
              },
              {
                featureType: 'poi.park',
                elementType: 'geometry.fill',
                stylers: [{color: '#f2f3ea'}]
              },
              {
                featureType: 'road',
                elementType: 'geometry',
                stylers: [{color: '#ffffff'}]
              },
              {
                featureType: 'road.arterial',
                elementType: 'geometry',
                stylers: [{color: '#ffffff'}]
              },
              {
                featureType: 'road.highway',
                elementType: 'geometry',
                stylers: [{color: '#ffffff'}]
              },
              {
                featureType: 'road.highway.controlled_access',
                elementType: 'geometry',
                stylers: [{color: '#ffffff'}]
              },
              {
                featureType: 'road.local',
                elementType: 'labels.text.fill',
                stylers: [{color: '#343A40'}]
              },
              {
                featureType: 'transit.line',
                elementType: 'geometry',
                stylers: [{color: '#ffffff'}]
              },
              {
                featureType: 'transit.line',
                elementType: 'labels.text.fill',
                stylers: [{color: '#343A40'}]
              },
              {
                featureType: 'transit.station',
                elementType: 'geometry',
                stylers: [{color: '#f2f3ea'}]
              },
              {
                featureType: 'water',
                elementType: 'geometry.fill',
                stylers: [{color: '#b9d3c2'}]
              },
              {
                featureType: 'water',
                elementType: 'labels.text.fill',
                stylers: [{color: '#343A40'}]
              }
            ], {name: 'Styled Map'});

            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: 13.7245601, lng: 100.4930267},
                zoom: 8,
                zoomControl: true,
                mapTypeControl: false,
                scaleControl: true,
                streetViewControl: false,
                rotateControl: false,
                fullscreenControl: true
            });
            // infoWindow = new google.maps.InfoWindow;
            //
            // // Try HTML5 geolocation.
            // if (navigator.geolocation) {
            //     navigator.geolocation.getCurrentPosition(function(position) {
            //         var pos = {
            //             lat: position.coords.latitude,
            //             lng: position.coords.longitude
            //         };
            //         infoWindow.setPosition(pos);
            //         infoWindow.setContent('Location found.');
            //         infoWindow.open(map);
            //         map.setCenter(pos);
            //     }, function() {
            //         handleLocationError(true, infoWindow, map.getCenter());
            //     });
            // } else {
            //     // Browser doesn't support Geolocation
            //     handleLocationError(false, infoWindow, map.getCenter());
            // }

            // var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

            // Add some markers to the map.
            // Note: The code uses the JavaScript Array.prototype.map() method to
            // create an array of markers based on a given "locations" array.
            // The map() method here has nothing to do with the Google Maps API.
            var markers = locations.map(function(location, i) {
              return new google.maps.Marker({
                position: location,
                label: null
              });
            });

            map.mapTypes.set('styled_map', styledMapType);
            map.setMapTypeId('styled_map');
            // Add a marker clusterer to manage the markers.

            mcOptions = {
              styles: [{
                  height: 66,
                  url: "https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m3.png",
                  width: 66
                },
              ]
            }


            var markerCluster = new MarkerClusterer(map, markers,mcOptions);
        }
        // function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        //     infoWindow.setPosition(pos);
        //     infoWindow.setContent(browserHasGeolocation ?
        //         'Error: The Geolocation service failed.' :
        //         'Error: Your browser doesn\'t support geolocation.');
        //     infoWindow.open(map);
        // }
    </script>
    <script src="https://unpkg.com/@google/markerclustererplus@4.0.1/dist/markerclustererplus.min.js"></script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBlEky4t6pYY_4MIbsvssO5aE1ecLK3kH0&callback=initMap">
    </script>
{{-- API Key =  AIzaSyBlEky4t6pYY_4MIbsvssO5aE1ecLK3kH0--}}
@endsection

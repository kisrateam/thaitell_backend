@extends('frontend.layouts.main')
@section('content')
<div class="bg-grad">
    <div>
        <div class="title" style="padding: 2em 1em 1em 1em">{{$latest->title}}</div>
        <img class="center-cropped"
             src="{{asset('/uploads/images/'.$latest->image)}}" >
        <div class="content hot-new">
            <p>{!!  $latest->short_detail !!}</p>
        </div>
        <div class="hot-new" style="align-self: end;padding-bottom: 2em; width: 100vw;display: flex">
            <div class="col-6"align="left" style="align-self: center;">
                <i class="far fa-clock"></i> {{$latest->date}}
            </div>
            <div class="col-6"align="right" >
                <div class="col-12 read-more" >
                    <a class="nav-link" style="color: black;" href="{{url('news/readmore/' . $latest->id)}}">อ่านต่อ</a>
                </div>
            </div>
        </div>
    </div>
    @foreach($data as $d)
        <div style="border-top: 0.5px solid #DADADA;padding: 1.5em 0.5em; width: 100vw;display: flex;flex-wrap: wrap">
            <div class="col-12 thumbnail">
                <img src="{{asset('/uploads/images/'.$d->image)}}" >
            </div>
            <div class="col-12">
                <div class="row col-12 title">
                    <p>{{$d->title}}</p>
                </div>
                {{--                <div class="row col-12 content crop">--}}
                {{--                    <p>{!!  $d->short_detail !!}</p>--}}
                {{--                </div>--}}
                <div class="row more" >
                    <div class="col-6"align="left"style="align-self: center;">
                        {{$d->date}}
                    </div>
                    <div class="col-6"align="right"style="align-self: center;">
                        <a class="nav-link @if($_SERVER['REQUEST_URI'] === 'news/readmore') active @endif" style="color: black;" href="{{url('news/readmore/'.$d->id)}}">อ่านต่อ <i class="fas fa-chevron-right" style="color: #D43731"></i></a>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>
    <style>
         .nav-link{padding: .5rem 1.5rem;}
         .thumbnail {
             width: 80vw;
             border-radius: 9px;
         }
         .thumbnail img {
             width: 100%;
             border-radius: 9px;
         }
         .thumbnail img.portrait {
             width: 100%;
             border-radius: 9px;
         }
        .content {
            font-family: Sarabun;
            font-style: normal;
            font-weight: 200;
            font-size: 12px;
            line-height: 14px;
        }
         .title{
             padding-top: 10px;
             font-style: normal;
             font-weight: bold;
             font-size: 18px;
             line-height: 21px;
         }
        .center-cropped {
            object-fit: cover;
            object-position: center; /* Center the image within the element */
            height: 250px;
            width: 100%;
        }
        .read-more{
            border: 1px solid #D43731;
            padding: 2px;
            border-radius: 20px;
            text-align: center;
        }
         .more{
             font-size: 11px;
             line-height: 11px;
         }
        .hot-new{
            padding: 5px 1em;
            font-size: 14px;
            line-height: 19px;
        }
        .bg-grad {
            background: linear-gradient(170.03deg, #FFFFFF -12.23%, #FFFFFF 40.51%, #DADADA 100.55%);
        }
        .hot-news-crop {
            overflow:hidden;
            height: 20vh;
            text-overflow:ellipsis;
            width:100%;
        }
        .hot-news-crop p{
            /*background: -webkit-linear-gradient(#333, #fff);*/
            background: -webkit-linear-gradient(#000000 -20%, #e7e7e7 35%);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
        }
        .title-crop {
            overflow:hidden;
            height: 20px;
            text-overflow:ellipsis;
            width:100%;
        }
        .crop {
            overflow:hidden;
            text-overflow:ellipsis;
            width:100%;
            height: 40px;
            margin-top: 1em;
            margin-bottom: 1em;
        }​


    </style>
@endsection

@extends('frontend.layouts.main')
@section('content')
    <div class="bg-grad">
        <div>
            <img class="center-cropped" src="{{asset('/uploads/images/'.$news->image)}}" >
            <div class="title" style="padding: 2em 1em 1em 1em">{{$news->title}}</div>
            <div class="row hot-new" style="align-self: end; width: 100vw">
                <div class="col-7"align="left" style="align-self: center;">
                    <i class="far fa-clock"></i> {{$news->date}}
                </div>
                <div class="col-5"align="right" style="padding-right: 1em">
                    <div class="share-button"  title="Share this article">
                        แชร์ข่าวสาร <i class="fas fa-share-alt"></i>
                    </div>
                </div>
            </div>
            <div class="content">
                <p>{!!  $news->detail !!}</p>
                <div style="padding: 1em;">
                    <div>อ้างอิงข้อมูลจาก</div>
                    <a class="row  nav-link" href="{{$news->source_url}}"><pre style="text-overflow: ellipsis;">{{$news->source_url}}</pre></a>
                </div>
            </div>
        </div>
        @if($data !== null)
            @foreach($data as $d)
                <div style="border-top: 0.5px solid #DADADA;padding: 1.5em 0.5em; width: 100vw;display: flex;flex-wrap: wrap">
                    <div class="col-12 thumbnail">
                        <img src="{{asset('/uploads/images/'.$d->image)}}" >
                    </div>
                    <div class="col-12">
                        <div class="row col-12 title">
                            <p>{{$d->title}}</p>
                        </div>
                        {{--                <div class="row col-12 content crop">--}}
                        {{--                    <p>{!!  $d->short_detail !!}</p>--}}
                        {{--                </div>--}}
                        <div class="row more" >
                            <div class="col-6"align="left"style="align-self: center;">
                                {{$d->date}}
                            </div>
                            <div class="col-6"align="right"style="align-self: center;">
                                <a class="nav-link" style="color: black;" href="{{url('news/readmore/'.$d->id)}}">อ่านต่อ <i class="fas fa-chevron-right" style="color: #D43731"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
    </div>

    <div class="share-dialog">
        <header>
            <h3 class="dialog-title">Share this website</h3>
            <button class="close-button"><svg><use href="#close"></use></svg></button>
        </header>
        <div class="targets">
            <a class="button" href="#!"
               onclick='PopupCenter("https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode("https://thaitell.com");?>//&t=<?php echo urlencode("THAI TELL")?>//","Share on facebook",400,400)'
               title="Share on facebook">
                <svg>
                    <use href="#facebook"></use>
                </svg>
                <span>Facebook</span>
            </a>
            <a class="button" href="#!"
               onclick='PopupCenter("http://twitter.com/home/?status=<?php echo urlencode("https://thaitell.com");?>","Share on Twitter",400,400)'
               title="Share on Twitter">
                <svg>
                    <use href="#twitter"></use>
                </svg>
                <span>Twitter</span>
            </a>
        </div>
        <div class="link">
            <div type="text" class="pen-url"><input type="text" value="https://thaitell.com" id="thaitell"></div>
            <button class="copy-link" onclick="copy()">Copy Link</button>
        </div>
    </div>

    <svg class="hidden">
        <defs>
            <symbol id="share-icon" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-share"><path d="M4 12v8a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2v-8"></path><polyline points="16 6 12 2 8 6"></polyline><line x1="12" y1="2" x2="12" y2="15"></line></symbol>

            <symbol id="facebook" viewBox="0 0 24 24" fill="#3b5998" stroke="#3b5998" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-facebook"><path d="M18 2h-3a5 5 0 0 0-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 0 1 1-1h3z"></path></symbol>

            <symbol id="twitter" viewBox="0 0 24 24" fill="#1da1f2" stroke="#1da1f2" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-twitter"><path d="M23 3a10.9 10.9 0 0 1-3.14 1.53 4.48 4.48 0 0 0-7.86 3v1A10.66 10.66 0 0 1 3 4s-4 9 5 13a11.64 11.64 0 0 1-7 2c9 5 20 0 20-11.5a4.5 4.5 0 0 0-.08-.83A7.72 7.72 0 0 0 23 3z"></path></symbol>

            <symbol id="close" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x-square"><rect x="3" y="3" width="18" height="18" rx="2" ry="2"></rect><line x1="9" y1="9" x2="15" y2="15"></line><line x1="15" y1="9" x2="9" y2="15"></line></symbol>
        </defs>
    </svg>

    <style>

        .hidden {
            display: none;
        }

        svg {
            width: 20px;
            height: 20px;
            margin-right: 7px;
        }

        button, .button {
            display: inline-flex;
            align-items: center;
            justify-content: center;
            height: auto;
            padding-top: 8px;
            padding-bottom: 8px;
            color: #777;
            text-align: center;
            font-size: 14px;
            font-weight: 300;
            line-height: 1.1;
            letter-spacing: 2px;
            text-transform: capitalize;
            text-decoration: none;
            white-space: nowrap;
            border-radius: 4px;
            border: 1px solid #ddd;
            cursor: pointer;
        }

        button:hover, .button:hover {
            border-color: #cdd;
        }

        .copy-link {
            padding-left: 30px;
            padding-right: 30px;
        }

        .share-dialog {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }

        .share-dialog {
            display: none;
            width: 95%;
            max-width: 500px;
            box-shadow: 0 8px 16px rgba(0,0,0,.15);
            z-index: -1;
            border: 1px solid #ddd;
            padding: 20px;
            border-radius: 4px;
            background-color: #fff;
        }

        .share-dialog.is-open {
            display: block;
            z-index: 2;
        }

        header {
            display: flex;
            justify-content: space-between;
            margin-bottom: 20px;
        }

        .targets {
            display: grid;
            grid-template-rows: 1fr;
            grid-template-columns: 1fr 1fr;
            grid-gap: 20px;
            margin-bottom: 20px;
        }

        .close-button {
            background-color: transparent;
            border: none;
            padding: 0;
        }

        .close-button svg {
            margin-right: 0;
        }

        .link {
            display: flex;
            justify-content: center;
            align-items: center;
            padding: 10px;
            border-radius: 4px;
            background-color: #eee;
        }

        .pen-url {
            margin-right: 15px;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
        }
        .thumbnail {
            width: 80vw;
            border-radius: 9px;
        }
        .thumbnail img {
            width: 100%;
            border-radius: 9px;
        }
        .thumbnail img.portrait {
            width: 100%;
            border-radius: 9px;
        }
        .content{
            font-family: Sarabun;
            font-style: normal;
            font-weight: 200;
            font-size: 14px;
            line-height: 19px;
            padding: 0px 2em 0px 2em;
        }
        .title{
            padding-top: 10px;
            font-style: normal;
            font-weight: bold;
            font-size: 18px;
            line-height: 21px;
        }
        .center-cropped {
            /*object-fit: cover; !* Do not scale the image *!*/
            object-position: center; /* Center the image within the element */
            /*height: 250px;*/
            width: 100%;
        }
        .more{
            font-size: 11px;
            line-height: 19px;
        }
        .hot-new{
            padding: 0px 0px 0px 2em;
            font-size: 11px;
            line-height: 19px;
        }
        .bg-grad {
            background: linear-gradient(170.03deg, #FFFFFF -12.23%, #FFFFFF 40.51%, #DADADA 100.55%);
        }
        .title-crop {
            overflow:hidden;
            height: 20px;
            text-overflow:ellipsis;
            width:100%;
        }
        .crop {
            overflow:hidden;
            text-overflow:ellipsis;
            width:100%;
            height: 40px;
            margin-top: 1em;
            margin-bottom: 1em;
        }​
    </style>
    <script>
        const shareButton = document.querySelector('.share-button');
        const shareDialog = document.querySelector('.share-dialog');
        const closeButton = document.querySelector('.close-button');

        shareButton.addEventListener('click', event => {
            if (navigator.share) {
                navigator.share({
                    title: 'WebShare API Demo',
                    url: 'https://codepen.io/ayoisaiah/pen/YbNazJ'
                }).then(() => {
                    console.log('Thanks for sharing!');
                })
                    .catch(console.error);
            } else {
                shareDialog.classList.add('is-open');
            }
        });

        closeButton.addEventListener('click', event => {
            shareDialog.classList.remove('is-open');
        });

        function PopupCenter(url, title, w, h) {
            // Fixes dual-screen position                         Most browsers      Firefox
            var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : window.screenX;
            var dualScreenTop = window.screenTop != undefined ? window.screenTop : window.screenY;
            var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;
            var systemZoom = width / window.screen.availWidth;
            var left = (width - w) / 2 / systemZoom + dualScreenLeft
            var top = (height - h) / 2 / systemZoom + dualScreenTop
            var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w / systemZoom + ', height=' + h / systemZoom + ', top=' + top + ', left=' + left);
            // Puts focus on the newWindow
            if (window.focus) newWindow.focus();
        }

        function copy() {
            /* Get the text field */
            var copyText = document.getElementById("thaitell");

            /* Select the text field */
            copyText.focus();
            copyText.select();
            copyText.setSelectionRange(0, 99999); /*For mobile devices*/

            /* Copy the text inside the text field */
            document.execCommand("copy");
        }
    </script>
@endsection

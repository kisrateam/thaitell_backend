@extends('frontend.layouts.main')
@section('content')
    <div class="bg-grad">
        <div align="center">
            <br>
            <p style="font-size: 14px; "><i class="far fa-clock"></i> อัพเดทเมื่อ {{$data['datetime']}}</p>
            <br>
            <br>
            <p style="font-size: 18px; font-weight: bold; line-height: 12px">จำนวนผู้ติดเชื้อสะสมทั้งหมด</p>
            <p style="color: #D95A69;font-size: 18px; font-weight: bold; line-height: 12px">ในประเทศไทย <img src="{{asset('/assets/images/Thailand1.png')}}" height="16" width="16"></p>
            <br>
        </div>

        <div align="center">
            <div class="col-4" style="text-align: center">
                <p style="color: #D43731;">COVID-19</p>
            </div>
            <div class="col-12 fon">
                <p>{{$data['sum']}}</p>
            </div>
            {{--            <p>(ติดเชื้อเพิ่มล่าสุด 32 ราย 16 มี.ค. 63)</p>--}}
        </div>
        <div align="center">
            <div class="justify-content-center bg-earth">
                <div class=" box1">
                    <p style="font-size: 16px; line-height: 35px;">คนไทย</p>
                    <p style="font-family: Arial;">{{$data['thai']}}</p>
                </div>
                <div class="col-1 col-sm-1">
                </div>
                <div class=" box2">
                    <p style="font-size: 16px; line-height: 35px;">ชาวต่างชาติ</p>
                    <p style="font-family: Arial;">{{$data['foreigner']}}</p>
                </div>
            </div>
        </div>
        <div class="detail">
            <div class="detail1">
            </div>
            <div class="detail2">
                @foreach($casesByStatus as $case)
                    <div class="justify-content-center" style="display: flex;">
                        <div class="col-4" align="center">{{$case['name']}}</div>
                        <div class="col-2" align="center">{{$case['y']}}</div>
                        <div class="col-3" align="center">ราย</div>
                    </div>
                @endforeach
            </div>
        </div>
        <div align="left">
            <div style="padding: 10% 10%; font-size: 14px;font-family: Sarabun;">
                <div>อ้างอิงข้อมูลจาก</div>
{{--                @foreach($urls as $url)--}}
{{--                    <a class="row nav-link" href="{{$url}}"><pre style="text-overflow: ellipsis;">{{$url}}</pre></a>--}}
{{--                @endforeach--}}
                <a style="color: #6c757d;"  href="https://www.bbc.com/thai">https://www.bbc.com/thai</a>
                <a style="color: #6c757d;"  href="https://www.infoquest.co.th/">https://www.infoquest.co.th</a>
                <a style="color: #6c757d;"  href="https://news.bectero.com">https://news.bectero.com</a>
            </div>
        </div>
    </div>

    <h3 align="center" class="title-bar">Total Coronavirus in Thailand</h3>
    <figure class="highcharts-figure">
        <div id="container1"></div>
    </figure>

    <h3 align="center" class="title-bar">Gender</h3>
    <figure class="highcharts-figure">
        <div id="container3"></div>
    </figure>

    <h3 align="center" class="title-bar">Nation</h3>
    <figure class="highcharts-figure">
        <div id="container4"></div>
    </figure>
    <h3 align="center" class="title-bar">Age</h3>
    <figure class="highcharts-figure">
        <div id="container5"></div>
    </figure>
{{--    <div class="bg-grad">--}}
{{--        <div align="left" style="border-top: 0.5px solid #DADADA;padding: 10% 10%">--}}
{{--            <div style="font-size: 14px; ">--}}
{{--                <div>อ้างอิงข้อมูลจาก</div>--}}
{{--                @foreach($urls as $url)--}}
{{--                    <a class="row nav-link" href="{{$url}}"><pre style="text-overflow: ellipsis;">{{$url}}</pre></a>--}}
{{--                @endforeach--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

    <style type="text/css">

        .box1 {
            font-style: normal;
            font-weight: bold;
            font-size: 48px;
            line-height: 25px;
            padding-bottom: 4px;

            color: #FFFFFF;
            text-align: center;
            background: #002952;
            border-radius: 12px;
            margin: 50px 0px;
            width: 120px;
        }

        .box2 {
            font-style: normal;
            font-weight: bold;
            font-size: 48px;
            line-height: 25px;
            padding-bottom: 4px;

            color: #FFFFFF;
            text-align: center;
            background: #D95A69;
            border-radius: 12px;
            margin: 50px 0px;
            width: 120px;
        }

        .detail {
            display: flex;
            width: 100%;
            height: 188px;

            justify-content: center;
            align-items: center;

            color: #FFFFFF;
            background: linear-gradient(0.91deg, #D43731 20.45%, #D95A69 131.4%);
        }

        .detail1 {
            background-image: url({{asset('/assets/images/thaimap.png')}});

            /* Full height */
            height: 100%;
            width: 100%;
            /* Center and scale the image nicely */
            background-position: right;
            background-repeat: no-repeat;
            opacity: 0.2;
            margin: 0 20%;
        }

        .bg-earth {
            display: flex;
            background-image: url({{asset('/assets/images/earth.png')}});

            /* Full height */
            height: 100%;

            /* Center and scale the image nicely */
            background-position: center;
            background-repeat: no-repeat;
        }

        .detail2 {
            width: 100%;
            position: absolute;
            /*z-index: 10;*/
            color: #FFFFFF;

            font-style: normal;
            font-weight: bold;
            font-size: 18px;
            line-height: 40px;
            padding-bottom: 4px;
        }

        .bg-grad {
            background: linear-gradient(170.03deg, #FFFFFF -12.23%, #FFFFFF 40.51%, #DADADA 100.55%);
        }

        .fon {
            font-family: Arial;
            font-style: normal;
            font-weight: bold;
            font-size: 96px;
            line-height: 70px;
            text-align: center;

            color: #002952;
        }

        .highcharts-figure, .highcharts-data-table table {
            min-width: 320px;
            max-width: 660px;
            margin: 1em auto;
        }

        .highcharts-data-table table {
            font-family: Verdana, sans-serif;
            border-collapse: collapse;
            border: 1px solid #EBEBEB;
            margin: 10px auto;
            text-align: center;
            width: 100%;
            max-width: 500px;
        }

        .highcharts-data-table caption {
            padding: 1em 0;
            font-size: 1.2em;
            color: #555;
        }

        .highcharts-data-table th {
            font-weight: 600;
            padding: 0.5em;
        }

        .highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
            padding: 0.5em;
        }

        .highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
            background: transparent;
        }

        .highcharts-data-table tr:hover {
            background: transparent;
        }

    </style>

    <script type="text/javascript">

        $(function () {
            $('#container1').highcharts(
                {!! json_encode($data['cases']) !!}
            );
        })

        $(function () {
            $('#container2').highcharts(
                {!! json_encode($data['cases']) !!}
            );
        })

        $(function () {
            var data = {!! json_encode($data['casesByGender']) !!}
            console.log(data);

            $('#container3').highcharts(
                {
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: null
                    },
                    colors: [ '#CADCF9',  '#D95A69','#E5E5E5' ],
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    accessibility: {
                        point: {
                            valueSuffix: '%'
                        }
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false
                            },
                            showInLegend: true
                        }
                    },
                    series: [{
                        name: 'Gender',
                        colorByPoint: true,
                        data: data
                    }]
                }
            );
        })

        $(function () {
            var data1 = {!! json_encode($data['casesByStatus']) !!}
            console.log(data1);
            var data = {!! json_encode($data['casesByNation']) !!}

            $('#container4').highcharts(
                {
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    colors: ['#D95A69', "#0091B9",  '#CADCF9', "#E5E5E5"],
                    title: {
                        text: null
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    accessibility: {
                        point: {
                            valueSuffix: '%'
                        }
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false
                            },
                            showInLegend: true
                        }
                    },
                    series: [{
                        name: 'Nation',
                        colorByPoint: true,
                        data: data
                    }]
                }
            );
        })

        $(function () {
            var data = {!! json_encode($data['casesByAge']) !!}
            console.log(data);
            Highcharts.chart('container5', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: null
                },
                subtitle: {
                    text: null
                },
                colors: ['#D95A69'],
                xAxis: {
                    type: 'category',
                    labels: {
                        rotation: 0,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: null
                    }
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    pointFormat: 'จำนวน <b>{point.y:.1f} คน</b>'
                },
                series: [{
                    name: 'age',
                    data: data,
                    dataLabels: {
                        enabled: false,
                        rotation: -90,
                        color: '#FFFFFF',
                        align: 'right',
                        format: '{point.y:.1f}', // one decimal
                        y: 10, // 10 pixels down from the top
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                }]
            });
        })
    </script>
@endsection


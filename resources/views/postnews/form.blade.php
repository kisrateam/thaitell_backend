<div class="form-ajax-box">
{!! Form::open(array('url'=>'postnews?return='.$return, 'class'=>'form-horizontal form-material validated','files' => true , 'parsley-validate'=>'','novalidate'=>' ','id'=> 'postnewsFormAjax')) !!}

	<div class="toolbar-nav">	
		<div class="row">	
			<div class="col-sm-6 ">	
				<button type="submit" class="btn btn-sm  " name="apply">{{ Lang::get('core.sb_apply') }} </button>
				<button type="submit" class="btn btn-sm  " name="save">  {{ Lang::get('core.sb_save') }} </button>
			</div>	
			<div class="col-md-6 text-right">
				<a href="javascript://ajax" onclick="ajaxViewClose('#{{ $pageModule }}')" class="tips btn btn-sm  " title="{{ __('core.btn_back') }}" ><i class="fa  fa-times"></i></a>				
			</div>
					
		</div>
	</div>	
		<div class="card">
			<div class="card-body">

	
	<div class="row"><div class="col-md-12">
						<fieldset><legend> PostNews</legend>
				{!! Form::hidden('id', $row['id']) !!}					
									  <div class="form-group row  " >
										<label for="Image" class=" control-label col-md-4 text-left"> Image </label>
										<div class="col-md-6">
										  
						
						<div class="custom-file mb-3 fileUpload">
			              <input type="file" class="custom-file-input upload" name="image"  a  accept="image/x-png,image/gif,image/jpeg"   >
			              <label class="custom-file-label form-control" for="customFile">Choose Image</label>
			            </div>
						<div class="image-preview preview-upload">
							{!! SiteHelpers::showUploadedFile( $row["image"],"/uploads/images") !!}
						</div>
					 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group row  " >
										<label for="Title" class=" control-label col-md-4 text-left"> Title </label>
										<div class="col-md-6">
										  <input  type='text' name='title' id='title' value='{{ $row['title'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group row  " >
										<label for="Short Detail" class=" control-label col-md-4 text-left"> Short Detail </label>
										<div class="col-md-6">
										  <textarea name='short_detail' rows='5' id='editor' class='form-control form-control-sm editor '  
						style= >{{ $row['short_detail'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group row  " >
										<label for="Detail" class=" control-label col-md-4 text-left"> Detail </label>
										<div class="col-md-6">
										  <textarea name='detail' rows='5' id='editor' class='form-control form-control-sm editor '  
						style="height: 500px;" >{{ $row['detail'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group row  " >
										<label for="Source Url" class=" control-label col-md-4 text-left"> Source Url </label>
										<div class="col-md-6">
										  <input  type='text' name='source_url' id='source_url' value='{{ $row['source_url'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group row  " >
										<label for="Source Url 2" class=" control-label col-md-4 text-left"> Source Url 2 </label>
										<div class="col-md-6">
										  <input  type='text' name='source_url_2' id='source_url_2' value='{{ $row['source_url_2'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group row  " >
										<label for="Update Date" class=" control-label col-md-4 text-left"> Update Date </label>
										<div class="col-md-6">
										  
					{!! Form::text('update_date', $row['update_date'],array('class'=>'form-control form-control-sm datetime')) !!}
				 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset></div></div>									
						
	<input type="hidden" name="action_task" value="save" />

	</div>
	

</div>		
{!! Form::close() !!}
</div>

<style type="text/css">
	.modal-body .form-ajax-box {
		margin: -15px;
	}
</style>
@include('sximo.module.template.ajax.formjavascript')
<script type="text/javascript">
$(document).ready(function() { 
	 
	
	 	
	 
	
	var form = $('#postnewsFormAjax'); 
	form.parsley();
	form.submit(function(){
		
		if(form.parsley().isValid()){			
			var options = { 
				dataType:      'json', 
				beforeSubmit :  function() {
				},
				success		:   function(data) {

					if(data.status == 'success')
					{
						ajaxViewClose('#{{ $pageModule }}');
						ajaxFilter('#{{ $pageModule }}','{{ $pageUrl }}/data');
						notyMessage(data.message);	
						$('#sximo-modal').modal('hide');	
					} else {
						notyMessageError(data.message);	
						return false;
					}	

				}  
			}  
			$(this).ajaxSubmit(options); 
			return false;
						
		} else {
			return false;
		}		
	
	});

});

</script>		 
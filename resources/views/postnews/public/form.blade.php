

		 {!! Form::open(array('url'=>'postnews/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="row"><div class="col-md-12">
						<fieldset><legend> PostNews</legend>
				{!! Form::hidden('id', $row['id']) !!}					
									  <div class="form-group row  " >
										<label for="Image" class=" control-label col-md-4 text-left"> Image </label>
										<div class="col-md-6">
										  
						
						<div class="custom-file mb-3 fileUpload">
			              <input type="file" class="custom-file-input upload" name="image"  a  accept="image/x-png,image/gif,image/jpeg"   >
			              <label class="custom-file-label form-control" for="customFile">Choose Image</label>
			            </div>
						<div class="image-preview preview-upload">
							{!! SiteHelpers::showUploadedFile( $row["image"],"/uploads/images") !!}
						</div>
					 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group row  " >
										<label for="Title" class=" control-label col-md-4 text-left"> Title </label>
										<div class="col-md-6">
										  <input  type='text' name='title' id='title' value='{{ $row['title'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group row  " >
										<label for="Short Detail" class=" control-label col-md-4 text-left"> Short Detail </label>
										<div class="col-md-6">
										  <textarea name='short_detail' rows='5' id='editor' class='form-control form-control-sm editor '  
						style= >{{ $row['short_detail'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group row  " >
										<label for="Detail" class=" control-label col-md-4 text-left"> Detail </label>
										<div class="col-md-6">
										  <textarea name='detail' rows='5' id='editor' class='form-control form-control-sm editor '  
						style="height: 500px;" >{{ $row['detail'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group row  " >
										<label for="Source Url" class=" control-label col-md-4 text-left"> Source Url </label>
										<div class="col-md-6">
										  <input  type='text' name='source_url' id='source_url' value='{{ $row['source_url'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group row  " >
										<label for="Source Url 2" class=" control-label col-md-4 text-left"> Source Url 2 </label>
										<div class="col-md-6">
										  <input  type='text' name='source_url_2' id='source_url_2' value='{{ $row['source_url_2'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group row  " >
										<label for="Update Date" class=" control-label col-md-4 text-left"> Update Date </label>
										<div class="col-md-6">
										  
					{!! Form::text('update_date', $row['update_date'],array('class'=>'form-control form-control-sm datetime')) !!}
				 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset></div></div>

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 

@if($setting['view-method'] =='native')
	<div class="toolbar-nav">
		<div class="row">
			<div class="col-md-6" >
				<a href="{{ ($prevnext['prev'] != '' ? url('postnews/'.$prevnext['prev'].'?return='.$return ) : '#') }}" class="tips btn btn-default btn-sm" onclick="ajaxViewDetail('#postnews',this.href); return false; "><i class="fa fa-arrow-left"></i>  </a>	
				<a href="{{ ($prevnext['next'] != '' ? url('postnews/'.$prevnext['next'].'?return='.$return ) : '#') }}" class="tips btn btn-default  btn-sm " onclick="ajaxViewDetail('#postnews',this.href); return false; "> <i class="fa fa-arrow-right"></i>  </a>
			</div>
			<div class="col-md-6 text-right pull-right" >
		   			

				<a href="javascript://ajax" onclick="ajaxViewClose('#{{ $pageModule }}')" class="tips btn btn-sm  " title="{{ __('core.btn_back') }}"><i class="fa  fa-times"></i></a>					
			</div>	

			
		</div>
	</div>	
		<div class="card">
		<div class="card-body">
@endif	

		<table class="table  table-bordered" >
			<tbody>	
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Id', (isset($fields['id']['language'])? $fields['id']['language'] : array())) }}</td>
						<td>{{ $row->id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Title', (isset($fields['title']['language'])? $fields['title']['language'] : array())) }}</td>
						<td>{{ $row->title}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Short Detail', (isset($fields['short_detail']['language'])? $fields['short_detail']['language'] : array())) }}</td>
						<td>{{ $row->short_detail}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Image', (isset($fields['image']['language'])? $fields['image']['language'] : array())) }}</td>
						<td>{{ $row->image}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Detail', (isset($fields['detail']['language'])? $fields['detail']['language'] : array())) }}</td>
						<td>{{ $row->detail}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Source Url', (isset($fields['source_url']['language'])? $fields['source_url']['language'] : array())) }}</td>
						<td>{{ $row->source_url}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Source Url 2', (isset($fields['source_url_2']['language'])? $fields['source_url_2']['language'] : array())) }}</td>
						<td>{{ $row->source_url_2}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Update Date', (isset($fields['update_date']['language'])? $fields['update_date']['language'] : array())) }}</td>
						<td>{{ $row->update_date}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Created At', (isset($fields['created_at']['language'])? $fields['created_at']['language'] : array())) }}</td>
						<td>{{ $row->created_at}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Updated At', (isset($fields['updated_at']['language'])? $fields['updated_at']['language'] : array())) }}</td>
						<td>{{ $row->updated_at}} </td>
						
					</tr>
				
			</tbody>	
		</table>  
			
		 	
		 
@if($setting['form-method'] =='native')
		</div>	
	</div>

@endif		